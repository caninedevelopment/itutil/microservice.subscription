﻿
namespace Unittests.Subscription.Command.Subscription
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Subscription;
    using Microservice.Subscription.Command.Subscription;
    using Microservice.Subscription.DTO;
    using ITUtil.Common.Base;
    using System.IO;

    [TestFixture]
    public class InsertTests
    {
        [Test]
        public void InsertOne_Success()
        {
            new Namespace(File.ReadAllText("madfaerdSchema.json"));
            Insert insert = new Insert();
            var subscriptionForInsert = new Microservice.Subscription.DTO.InsertSubscription()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                Items = new List<string>(){ "{\"person\": \"Kasper\"," +
                                "\"school\": \"Assens Realskole\"," +
                                "\"class\": \"5c\"," +
                                "\"vegetarian\": false," +
                                "\"lactoseIntolerant\": false}" },
                SubscriptionStatusCode = Microservice.Subscription.SubscriptionStatusCode.Active,
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),
            };
            insert.Execute(subscriptionForInsert);
        }
        [Test]
        public void InsertOneWithMissingId_Error() //TODO should there be a test for every "required" property
        {
            Insert insert = new Insert();
            var subscriptionForInsert = new Microservice.Subscription.DTO.InsertSubscription();
            NullOrDefaultException ex = Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => insert.Execute(subscriptionForInsert));
            Assert.That(ex.Message, Is.EqualTo("Id was not provided"));
        }

        [Test]
        public void InsertOneWhereSubscriptionAlreadyExists_Error()
        {
            #region preparation
            Insert insert = new Insert();
            var subscriptionForInsert = new Microservice.Subscription.DTO.InsertSubscription()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                Items = new List<string>() { "{\"person\": \"Kasper\"," +
                                "\"school\": \"Assens Realskole\"," +
                                "\"class\": \"5c\"," +
                                "\"vegetarian\": false," +
                                "\"lactoseIntolerant\": false}" },
                SubscriptionStatusCode = Microservice.Subscription.SubscriptionStatusCode.Active,
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),
            };
            insert.Execute(subscriptionForInsert);
            #endregion
            var subscriptionThatAlreadyExists = subscriptionForInsert;
            ElementAlreadyExistsException ex = Assert.Throws<ITUtil.Common.Base.ElementAlreadyExistsException>(() => insert.Execute(subscriptionThatAlreadyExists));
            Assert.That(ex.Message, Is.EqualTo("Subscription already exists"));
        }
    }
}
