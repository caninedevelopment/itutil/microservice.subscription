﻿namespace Unittests.Subscription.Command.Subscription
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Subscription;
    using Microservice.Subscription.Command.Subscription;
    using Microservice.Subscription.DTO;
    using ITUtil.Common.Base;

    [TestFixture]
    public class GetByUserIdTests
    {
        [Test]
        public void GetByUserId_Success()
        {
            #region Preparation
            Insert insert = new Insert();
            var subscriptionForInsert = new Microservice.Subscription.DTO.InsertSubscription()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                Items = new List<string>()
                {

                "{\"persons\": \"Kasper\"," +
                                "\"school\": \"Assens Realskole\"," +
                                "\"class\": \"5c\"," +
                                "\"vegetarian\": false," +
                                "\"lactoseIntolerant\": false}" 
                }
                ,
                SubscriptionStatusCode = Microservice.Subscription.SubscriptionStatusCode.Active,
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),
            };
            insert.Execute(subscriptionForInsert);
            #endregion
            ByExternalId byUserId = new ByExternalId
            {
                ExternalId = subscriptionForInsert.ExternalUser.Id
            };

            GetAllByUserId getByUserId = new GetAllByUserId();
            getByUserId.Execute(byUserId);
        }
    }
}
