﻿
namespace Unittests.Subscription.Command.Subscription
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;
    using Microservice.Subscription;
    using Microservice.Subscription.Command.Subscription;
    using Microservice.Subscription.DTO;
    using ITUtil.Common.Base;
    using System.IO;

    [TestFixture]
    public class UpdateTests
    {
        [Test]
        public void UpdateOne_Success()
        {
            #region Preparation
            new Namespace(File.ReadAllText("madfaerdSchema.json"));
            Insert insert = new Insert();
            var subscriptionForInsert = new Microservice.Subscription.DTO.InsertSubscription()
            {
                Id = Guid.NewGuid(),
                ExternalUser = new ExternalReference("UserNr1", new ExternalReference.PathDefinition("Microservice.User", ExternalReference.PathDefinition.TypeDefinition.RabbitMq)),
                Items = new List<string>() { "{\"person\": \"Kasper\"," +
                                "\"school\": \"Assens Realskole\"," +
                                "\"class\": \"5c\"," +
                                "\"vegetarian\": false," +
                                "\"lactoseIntolerant\": false}" },
                SubscriptionStatusCode = Microservice.Subscription.SubscriptionStatusCode.Active,
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),

            };
            insert.Execute(subscriptionForInsert);
            #endregion
            //TODO FIX: ikke samme DTO mere
            //UpdateCustomFields update = new UpdateCustomFields();
            //var subscriptionForUpdate = subscriptionForInsert;
            //subscriptionForUpdate.CustomFields = "{\"person\": \"Kasper\"," +
            //                    "\"school\": \"Assens Realskole\"," +
            //                    "\"class\": \"5c\"," +
            //                    "\"vegetarian\": false," +
            //                    "\"lactoseIntolerant\": false}";
            //subscriptionForUpdate.StartDate = DateTime.UtcNow.AddDays(15);
            //subscriptionForUpdate.EndDate = DateTime.UtcNow.AddDays(45);
            //update.Execute(subscriptionForUpdate);
        }
    }

}
