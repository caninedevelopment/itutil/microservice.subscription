﻿using System.IO;

namespace Microservice.Subscription
{
        class Program
        {
            static void Main(string[] args)
            {
                ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                (File.Exists("CustomFieldsSchema.json") ? new Command.Subscription.Namespace(File.ReadAllText("CustomFieldsSchema.json")) : new Command.Subscription.Namespace())
            });
            }
        }
    }
