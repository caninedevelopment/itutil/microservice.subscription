﻿
namespace Microservice.Subscription
{
    public enum SubscriptionStatusCode
    {
        NotSet = 0,
        Active = 100,
        OnHold = 200,
        Cancelled = 300,
        Expired = 400,
        PendingCancellation = 500,
        PendingActivation = 510
    }
}
