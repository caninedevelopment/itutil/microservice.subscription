﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Subscription.DTO
{
    public class GetSchema
    {
        public string Schema { get; set; }
    }
}
