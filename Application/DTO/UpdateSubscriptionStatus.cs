﻿
namespace Microservice.Subscription.DTO
{
    using System;

    public class UpdateSubscriptionStatus
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets SubscriptionStatusCode.
        /// </summary>
        public SubscriptionStatusCode SubscriptionStatusCode { get; set; }
    }

    //get my subscriptions (no claims)

}
