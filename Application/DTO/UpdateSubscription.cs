﻿
namespace Microservice.Subscription.DTO
{
    using System;
    using System.Collections.Generic;

    public class UpdateSubscription
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets CustomFields.
        /// </summary>
        public List<string> Items { get; set; } // Todo add validation up against a jsonSchema that has to be provided on setup
    }

    //get my subscriptions (no claims)

}
