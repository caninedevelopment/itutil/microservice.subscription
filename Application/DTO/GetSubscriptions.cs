﻿
namespace Microservice.Subscription.DTO
{
    using System.Collections.Generic;

    public class GetSubscriptions
    {
        public List<GetSubscription> list { get; set; }
    }

    //get my subscriptions (no claims)

}
