﻿namespace Microservice.Subscription.DTO
{
    using System;
    public class ById
    {
        public Guid Id { get; set; }
    }
}
