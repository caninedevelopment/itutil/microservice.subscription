﻿
namespace Microservice.Subscription.DTO
{
    using ITUtil.Common.Base;
    using System;
    using System.Collections.Generic;

    public class InsertOwnSubscription
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets CustomFields.
        /// </summary>
        public List<string> Items { get; set; } // Todo add validation up against a jsonSchema that has to be provided on setup
        /// <summary>
        /// Gets or sets SubscriptionStatusCode.
        /// </summary>
        public SubscriptionStatusCode SubscriptionStatusCode { get; set; }
        /// <summary>
        /// Gets or sets StartDate.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets EndDate.
        /// </summary>
        public DateTime? EndDate { get; set; }
    }

    //get my subscriptions (no claims)

}
