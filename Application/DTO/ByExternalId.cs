﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.Subscription.DTO
{
    public class ByExternalId
    {
        public string ExternalId { get; set; }
    }
}
