﻿
namespace Microservice.Subscription.DTO
{
    using ITUtil.Common.Base;
    using Microservice.Subscription.Database;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class CancelAllByReference
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }


    }

    //get my subscriptions (no claims)

}
