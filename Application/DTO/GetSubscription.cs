﻿
namespace Microservice.Subscription.DTO
{
    using ITUtil.Common.Base;
    using Microservice.Subscription.Database;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class GetSubscription
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets ExternalUser.
        /// </summary>
        public ExternalReference ExternalUser { get; set; }
        /// <summary>
        /// Gets or sets Items.
        /// </summary>
        public List<string> Items { get; set; }
        /// <summary>
        /// Gets or sets SubscriptionStatusCode.
        /// </summary>
        public SubscriptionStatusCode SubscriptionStatusCode { get; set; }
        /// <summary>
        /// Gets or sets StartDate.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets EndDate.
        /// </summary>
        public DateTime? EndDate { get; set; }

        public GetSubscription() { }

        public GetSubscription(Database.Subscription subscription) 
        {
            if (subscription.Items != null)
                this.Items = subscription.Items.Select(p => p.CustomFields).ToList();
            else
                this.Items = new List<string>();

            this.EndDate = subscription.EndDate;
            //this.ExternalOrderHistory = subscription.ExternalOrderHistory;
            //this.ExternalPayment = subscription.ExternalPayment;
            this.ExternalUser = subscription.ExternalUser;
            this.Id = subscription.Id;
            this.StartDate = subscription.StartDate;
            this.SubscriptionStatusCode = subscription.SubscriptionStatusCode;
        }

    }

    //get my subscriptions (no claims)

}
