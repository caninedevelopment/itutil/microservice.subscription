﻿using NJsonSchema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using NJsonSchema.Validation;

namespace Microservice.Subscription
{
    public static class CustomFieldValidator
    {
        public static List<string> ValidateCustomFields(string json, string jsonSchema)
        {
            JsonSchema jSchema = new JsonSchema();
            if (jsonSchema != null)
            {
                jSchema = JsonSchema.FromJsonAsync(jsonSchema).Result;
            }
            List<string> errorMessages = new List<string>();
            List<ValidationError> validationErrors = jSchema.Validate(json).ToList();
            validationErrors.ForEach(validationError =>
            {
                errorMessages.Add($"{validationError.Kind}: {validationError.Property}. Path: '{validationError.Path}'. line {validationError.LineNumber}, position {validationError.LinePosition}.");
            });
            return errorMessages;
        }
    }
}
