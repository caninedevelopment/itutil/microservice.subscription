﻿namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    public class UpdateStatus : UpdateCommand<DTO.UpdateSubscriptionStatus>
    {
        public UpdateStatus() : base("Update a subscriptions status, setting the enddate if the new status is cancelled or expired")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UpdateSubscriptionStatus input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription == null)
            {
                throw new ElementDoesNotExistException("Subscription does not exist", input.Id.ToString());
            }

            Subscription.SubscriptionStatusCode = input.SubscriptionStatusCode;
            if (input.SubscriptionStatusCode == SubscriptionStatusCode.Cancelled ||
                input.SubscriptionStatusCode == SubscriptionStatusCode.Expired)
            {
                Subscription.EndDate = DateTime.Now;
            }
            Namespace.db.UpdateSubscription(input.Id, Subscription);
        }
    }
}
