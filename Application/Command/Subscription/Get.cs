﻿
namespace Microservice.Subscription.Command.Subscription
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;
    public class Get : GetCommand<DTO.ById, DTO.GetSubscription>
    {
        public Get() : base("Get subscription by id")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override DTO.GetSubscription Execute(DTO.ById input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription == null)
            {
                throw new ElementDoesNotExistException("Subscription does not exist", input.Id.ToString());
            }
            return new DTO.GetSubscription(Subscription);
        }
    }
}
