﻿
namespace Microservice.Subscription.Command.Subscription
{
    using System.Linq;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    public class GetAllByUserId : GetCommand<DTO.ByExternalId, DTO.GetSubscriptions>
    {
        public GetAllByUserId() : base("Get subscriptions by userId")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override DTO.GetSubscriptions Execute(DTO.ByExternalId input)
        {
            DTO.GetSubscriptions result = new DTO.GetSubscriptions();

            if (string.IsNullOrEmpty(input.ExternalId))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("ExternalId was not provided", "ExternalId");
            }
            var Subscriptions = Namespace.db.GetSubscriptionsByUserId(input.ExternalId);
            if (Subscriptions.Count == 0)
            {
                throw new ElementDoesNotExistException("Subscriptions do not exist", input.ExternalId.ToString());
            }
            result.list = Subscriptions.Select(p => new DTO.GetSubscription(p)).ToList();
            return result;
        }
    }
}
