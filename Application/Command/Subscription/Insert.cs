﻿
namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Insert : InsertCommand<DTO.InsertSubscription>
    {
        public Insert() : base("Insert a subscription")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.InsertSubscription input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription != null)
            {
                throw new ElementAlreadyExistsException("Subscription already exists", input.Id.ToString());
            }
            Subscription = new Database.Subscription()
            {
                Id = input.Id,
                ExternalUser = input.ExternalUser,
                Items = new System.Collections.Generic.List<Database.SubscriptionLine>(),
                SubscriptionStatusCode = input.SubscriptionStatusCode,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
            };
            foreach (var cf in input.Items)
            {
                Subscription.Items.Add(new Database.SubscriptionLine() { CustomFields = cf });
            }

            Namespace.db.InsertSubscription(Subscription);
        }
    }
}
