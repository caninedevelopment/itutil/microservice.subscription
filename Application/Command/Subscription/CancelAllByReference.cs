﻿namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;

    public class CancelAllByReference : UpdateCommand<DTO.CancelAllByReference>
    {
        public CancelAllByReference() : base("Cancel all subscription from same user as the provided subscription")
        {
            this.Claims.Add(Namespace.IsAdmin);
            this.Claims.Add(Namespace.IsCustomer);
        }
        public override void Execute(DTO.CancelAllByReference input, object args, string initiatedByIP)
        {
            var credentials = args as TokenInfo;

            var Subscriptions = Namespace.db.GetSubscriptionByReference(input.Id);
            foreach (var sub in Subscriptions)
            {
                if (sub.SubscriptionStatusCode != SubscriptionStatusCode.Cancelled)
                {
                    sub.SubscriptionStatusCode = SubscriptionStatusCode.Cancelled;
                    sub.EndDate = DateTime.Now;
                    Namespace.db.UpdateSubscription(sub.Id, sub);
                }
            }
        }
    }
}
