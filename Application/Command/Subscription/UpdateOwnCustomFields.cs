﻿namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;

    public class UpdateOwnCustomFields : UpdateCommand<DTO.UpdateSubscription>
    {
        public UpdateOwnCustomFields() : base("Update the custom fields on own subscription")
        {
            this.Claims.Add(Namespace.IsAdmin);
            this.Claims.Add(Namespace.IsCustomer);
        }
        public override void Execute(DTO.UpdateSubscription input, object args, string initiatedByIP)
        {
            var credentials = args as TokenInfo;
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription == null || Subscription.ExternalUser.Id != credentials.userId.ToString())
            {
                throw new ElementDoesNotExistException("Subscription does not exist", input.Id.ToString());
            }
            Subscription.Items = new System.Collections.Generic.List<Database.SubscriptionLine>();
            foreach (var cf in input.Items)
            {
                Subscription.Items.Add(new Database.SubscriptionLine() {  CustomFields = cf });
            }
            Namespace.db.UpdateSubscription(input.Id, Subscription);
        }
    }
}