﻿
namespace Microservice.Subscription.Command.Subscription
{
    using ITUtil.Common.Command;
    using System.IO;

    public class GetcustomFieldsSchema : GetCommand<object, DTO.GetSchema>
    {
        public GetcustomFieldsSchema() : base("Get custom fields schema definition")
        {
        }

        public override DTO.GetSchema Execute(object input)
        {
            return new DTO.GetSchema() { Schema = File.ReadAllText("CustomFieldsSchema.json") };
        }
    }
}
