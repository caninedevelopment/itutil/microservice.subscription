﻿
namespace Microservice.Subscription.Command.Subscription
{
    using ITUtil.Common.Command;
    using System.Linq;

    public class GetAll : GetCommand<object, DTO.GetSubscriptions>
    {
        public GetAll() : base("Get all subscriptions")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override DTO.GetSubscriptions Execute(object input = null) // Todo Maybe write another overload with no arguments in the GetCommand?
        {
            DTO.GetSubscriptions result = new DTO.GetSubscriptions();
            result.list = Namespace.db.GetSubscriptions().Select(p=> new DTO.GetSubscription(p)).ToList();
            return result;
        }
    }
}
