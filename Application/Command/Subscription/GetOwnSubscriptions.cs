﻿namespace Microservice.Subscription.Command.Subscription
{
    using System.Linq;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;
    using Microservice.Subscription.DTO;

    public class GetOwnSubscriptions : GetCommand<object, DTO.GetSubscriptions>
    {
        public GetOwnSubscriptions() : base("Get all my own subscriptions based on token")
        {
            this.Claims.Add(Namespace.IsAdmin);
            this.Claims.Add(Namespace.IsCustomer);
        }

        public override GetSubscriptions Execute(object input, object args, string initiatedByIP)
        {
            var credentials = args as TokenInfo;
            
            DTO.GetSubscriptions result = new DTO.GetSubscriptions();
            result.list = Namespace.db.GetSubscriptionsByUserId(credentials.userId.ToString()).Select(p => new DTO.GetSubscription(p)).ToList();
            return result;
        }

    }
}