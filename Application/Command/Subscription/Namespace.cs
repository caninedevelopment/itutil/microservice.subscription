﻿namespace Microservice.Subscription.Command.Subscription
{
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Namespace : BaseNamespace
    {
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. can change user rights") }, key = "Monosoft.Subscription.isAdmin" };
        internal static Claim IsCustomer = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is a customer, i.e. can create own subscriptions") }, key = "Monosoft.Subscription.isCustomer" };

        public static DataContext db = new DataContext();
        public static string SchemaForMappingCustomFieldsOnSubscription { get; set; }
        public Namespace(string schemaForMappingCustomFieldsOnSubscription = null) : base("MongoDb_Subscription", new ProgramVersion("1.0.0.0"))
        {
            SchemaForMappingCustomFieldsOnSubscription = schemaForMappingCustomFieldsOnSubscription;
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Delete(),
                new Get(),
                new GetAll(),
                new GetAllByUserId(),
                new GetcustomFieldsSchema(),
                new GetOwnSubscriptions(),
                new Insert(),
                new InsertOwnSubscription(),
                new UpdateOwnCustomFields(),
                new UpdateOwnStatus(),
                new UpdateCustomFields(),
                new UpdateStatus(),
                new CancelAllOwn(),
                new CancelAllByReference()
            });
        }

    }
}
