﻿namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;

    public class UpdateOwnStatus : UpdateCommand<DTO.UpdateSubscriptionStatus>
    {
        public UpdateOwnStatus() : base("Update own subscription, but only if it is not already cancelled or expired. Enddate will also be set, if the status changes to cancelled or expired")
        {
            this.Claims.Add(Namespace.IsAdmin);
            this.Claims.Add(Namespace.IsCustomer);
        }
        public override void Execute(DTO.UpdateSubscriptionStatus input, object args, string initiatedByIP)
        {
            var credentials = args as TokenInfo;

            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription == null || Subscription.ExternalUser.Id != credentials.userId.ToString())
            {
                throw new ElementDoesNotExistException("Subscription does not exist", input.Id.ToString());
            }

            switch (input.SubscriptionStatusCode)
            {
                case SubscriptionStatusCode.NotSet:
                    throw new NullOrDefaultException("SubscriptionStatusCode was not provided", "SubscriptionStatusCode");
                case SubscriptionStatusCode.Active:
                case SubscriptionStatusCode.OnHold:
                case SubscriptionStatusCode.Cancelled:
                case SubscriptionStatusCode.Expired:
                case SubscriptionStatusCode.PendingCancellation:
                    if (Subscription.SubscriptionStatusCode == SubscriptionStatusCode.Active ||
                        Subscription.SubscriptionStatusCode == SubscriptionStatusCode.OnHold ||
                        Subscription.SubscriptionStatusCode == SubscriptionStatusCode.PendingCancellation)
                    {
                        Subscription.SubscriptionStatusCode = input.SubscriptionStatusCode;

                        if (input.SubscriptionStatusCode == SubscriptionStatusCode.Cancelled ||
                            input.SubscriptionStatusCode == SubscriptionStatusCode.Expired)
                        {
                            Subscription.EndDate = DateTime.Now;
                        }

                        Namespace.db.UpdateSubscription(input.Id, Subscription);
                    }
                    else
                    {
                        throw new NullOrDefaultException("SubscriptionStatusCode can not be set to active due to its current status: " + Subscription.SubscriptionStatusCode, "SubscriptionStatusCode");//TODO: FORKERT EXCEPTION TYPE
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
