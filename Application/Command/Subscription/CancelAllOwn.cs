﻿namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;

    public class CancelAllOwn : UpdateCommand<object>
    {
        public CancelAllOwn() : base("Cancel all own subscription ")
        {
            this.Claims.Add(Namespace.IsAdmin);
            this.Claims.Add(Namespace.IsCustomer);
        }
        public override void Execute(object input, object args, string initiatedByIP)
        {
            var credentials = args as TokenInfo;

            var Subscriptions = Namespace.db.GetSubscriptionsByUserId(credentials.userId.ToString());
            foreach (var sub in Subscriptions)
            {
                if (sub.SubscriptionStatusCode != SubscriptionStatusCode.Cancelled)
                {
                    sub.SubscriptionStatusCode = SubscriptionStatusCode.Cancelled;
                    sub.EndDate = DateTime.Now;
                    Namespace.db.UpdateSubscription(sub.Id, sub);
                }
            }
        }
    }
}
