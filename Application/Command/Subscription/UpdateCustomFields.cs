﻿namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    public class UpdateCustomFields : UpdateCommand<DTO.UpdateSubscription>
    {
        public UpdateCustomFields() : base("Update custom fields on a subscription")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UpdateSubscription input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }
            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription == null)
            {
                throw new ElementDoesNotExistException("Subscription does not exist", input.Id.ToString());
            }
            Subscription.Items = new System.Collections.Generic.List<Database.SubscriptionLine>();
            foreach (var cf in input.Items)
            {
                Subscription.Items.Add(new Database.SubscriptionLine() { CustomFields = cf });
            }

            Namespace.db.UpdateSubscription(input.Id, Subscription);
        }
    }
}