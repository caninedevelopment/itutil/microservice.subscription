﻿
namespace Microservice.Subscription.Command.Subscription
{
    using ITUtil.Common.Command;
    using System;
    public class Delete : DeleteCommand<DTO.ById>
    {
        public Delete() : base("Permantly delete a subscription")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.ById input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            var Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription == null)
            {
                throw new ITUtil.Common.Base.ElementDoesNotExistException("Subscription does not exist", input.Id.ToString());
            }
            Namespace.db.DeleteSubscription(input.Id);
        } 
    }
}
