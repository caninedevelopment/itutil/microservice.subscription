﻿
namespace Microservice.Subscription.Command.Subscription
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ.DTO;

    public class InsertOwnSubscription : InsertCommand<DTO.InsertOwnSubscription>
    {
        public InsertOwnSubscription() : base("Insert subscription for the current user (based on tokenid)")
        {
            this.Claims.Add(Namespace.IsAdmin);
            this.Claims.Add(Namespace.IsCustomer);
        }

        public override void Execute(DTO.InsertOwnSubscription input, object args, string initiatedByIP)
        {
            var credentials = args as TokenInfo;

            if (input.Id == Guid.Empty)
            {
                throw new NullOrDefaultException("Id was not provided", "Id");
            }

            Database.Subscription Subscription = Namespace.db.GetSubscriptionById(input.Id);
            if (Subscription != null)
            {
                throw new ElementAlreadyExistsException("Subscription already exists", input.Id.ToString());
            }

            Subscription = new Database.Subscription()
            {
                Id = input.Id,
                ExternalUser = new ExternalReference() 
                { 
                    Id = credentials.userId.ToString(), 
                    Path= new ExternalReference.PathDefinition() 
                    { 
                        Type= ExternalReference.PathDefinition.TypeDefinition.RabbitMq, 
                        Uri="User" 
                    } 
                },
                Items = new List<Database.SubscriptionLine>(),
                SubscriptionStatusCode = input.SubscriptionStatusCode,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
            };
            foreach (var cf in input.Items)
            {
                Subscription.Items.Add(new Database.SubscriptionLine() { CustomFields = cf });
            }
            Namespace.db.InsertSubscription(Subscription);
        }
    }
}
