﻿namespace Microservice.Subscription
{
    using System;
    using System.Collections.Generic;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Conventions;
    using MongoDB.Driver;

    /// <summary>
    /// Datacontext for the orderdatabase.
    /// </summary>
    public class DataContext
    {
        private readonly string dbName = "subscriptionDatabase";
        private readonly string collectionName = "subscription";
        private readonly IMongoDatabase mongoDb;
        /// <summary>
        /// Initializes a new instance of the <see cref="DataContext"/> class.
        /// </summary>
        public DataContext()
        {
            var customConventions = new ConventionPack {
                new IgnoreExtraElementsConvention(true), // Ignore the extra fields that a document has when compared to the provided DTO on Deserialization.  
                new IgnoreIfDefaultConvention(true) // Ignore the fields with default value on Serialization. Keeps the document smaller.
            };
            ConventionRegistry.Register("CustomConventions", customConventions, type => true); // how to handle different encounters during serialization & deserialization.
            var settings = ITUtil.Common.Config.GlobalRessources.getConfig();
            if (settings == null)
            {
                throw new Exception("GlobalRessource settings were not provided");
            }
            var NOSQL = settings.GetSetting<ITUtil.Common.Config.NoSQLSettings>();
            var client = new MongoClient(NOSQL.ConnectionString());
            mongoDb = client.GetDatabase(NOSQL.GetFullDBName(dbName)); // automatically creates a database if it doesn't exists
        }
        public void InsertSubscription(Database.Subscription document) 
        {
            var subscriptionCollection = this.mongoDb.GetCollection<Database.Subscription>(collectionName);
            subscriptionCollection.InsertOne(document);
        }

        public List<Database.Subscription> GetSubscriptions()
        {
            var subscriptionCollection = mongoDb.GetCollection<Database.Subscription>(collectionName);
            return subscriptionCollection.Find(subscription => true).ToList();
        }

        public List<Database.Subscription> GetSubscriptionByReference(Guid id)
        {
            var userId = this.GetSubscriptionById(id).ExternalUser.Id;
            var subscriptionCollection = mongoDb.GetCollection<Database.Subscription>(collectionName);
            var filter = Builders<Database.Subscription>.Filter.Eq("ExternalUser.Id", userId);
            return subscriptionCollection.Find(filter).ToList();
        }
        public List<Database.Subscription> GetSubscriptionsByUserId(string userId)
        {
            var subscriptionCollection = mongoDb.GetCollection<Database.Subscription>(collectionName);
            var filter = Builders<Database.Subscription>.Filter.Eq("ExternalUser.Id", userId);
            return subscriptionCollection.Find(filter).ToList();
        }
        public Database.Subscription GetSubscriptionById(Guid id)
        {
            var subscriptionCollection = mongoDb.GetCollection<Database.Subscription>(collectionName);
            var filter = Builders<Database.Subscription>.Filter.Eq("Id", id);
            return subscriptionCollection.Find(filter).FirstOrDefault();
        }

        public void UpdateSubscription(Guid id, Database.Subscription subscription)
        {
            var subscriptionCollection = mongoDb.GetCollection<Database.Subscription>(collectionName);
            var filter = Builders<Database.Subscription>.Filter.Eq("Id", id);
            subscriptionCollection.ReplaceOne(
                filter,
                subscription
            );
        }

        public void DeleteSubscription(Guid id)
        {
            var subscriptionCollection = mongoDb.GetCollection<Database.Subscription>(collectionName);
            var filter = Builders<Database.Subscription>.Filter.Eq("Id", id);
            subscriptionCollection.DeleteOne(filter);
        }

    }
}
