﻿
namespace Microservice.Subscription.Database
{
    using ITUtil.Common.Base;
    using Microservice.Subscription.Command.Subscription;
    using Microservice.Subscription.DTO;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    public class Subscription
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [BsonId] // converts the Property to _id. Auto generates Guid.
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets ExternalUser.
        /// </summary>
        private ExternalReference externalUser { get; set; }
        public ExternalReference ExternalUser {
            get { return externalUser; }
            set {
                if (value == null)
                {
                    throw new NullOrDefaultException("ExternalUser was not provided", "ExternalUser");
                }
                externalUser = value;
            }
        }


        public List<SubscriptionLine> Items { get; set; }





        /// <summary>
        /// Gets or sets SubscriptionStatusCode.
        /// </summary>
        public SubscriptionStatusCode subscriptionStatusCode;
        public SubscriptionStatusCode SubscriptionStatusCode
        {
            get { return subscriptionStatusCode; }
            set {
                if (value == SubscriptionStatusCode.NotSet)
                {
                    throw new NullOrDefaultException("SubscriptionStatusCode was not provided", "SubscriptionStatusCode");
                }

                subscriptionStatusCode = value;
            }
        }
        /// <summary>
        /// Gets or sets StartDate.
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets EndDate.
        /// </summary>
        public DateTime? EndDate { get; set; }

    //    /// <summary>
    //    /// Gets or sets ExternalPayment.
    //    /// </summary>
    //    private ExternalReference externalPayment;
    //    public ExternalReference ExternalPayment 
    //    {
    //        get { return externalPayment; } 
    //        set
    //        {
    //            if (value == null)
    //            {
    //                throw new NullOrDefaultException("ExternalPayment was not provided", "ExternalPayment");
    //            }
    //            externalPayment = value;
    //        }
    //    }


    ///// <summary>
    ///// Gets or sets OrderHistory.
    ///// </summary>
    //public List<ExternalReference> ExternalOrderHistory { get; set; }

    }

    public class SubscriptionLine
    {
        /// <summary>
        /// Gets or sets CustomFields.
        /// </summary>
        private string customFields;
        /// <summary>
        /// Gets or sets CustomFields.
        /// </summary>
        public string CustomFields
        {
            get { return customFields; }
            set
            {

                List<string> errorMessages = CustomFieldValidator.ValidateCustomFields(value, Namespace.SchemaForMappingCustomFieldsOnSubscription);
                if (errorMessages.Count > 0)
                {
                    throw new ValidationException(errorMessages.FirstOrDefault(), string.Join(",", errorMessages));
                }
                customFields = value;
            }

        } // Todo add validation up against a jsonSchema that has to be provided on setup
    }
}
